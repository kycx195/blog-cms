<article class="post-comments" id="post-comments">
    <h3><i class="fa fa-comments"></i> {{ $post->commentsNumber('Comment')}}</h3>

    <div class="comment-body padding-10">
        <ul class="comments-list">
            @foreach($post->comments as $comment)
                <li class="comment-item">
                    <div class="comment-heading clearfix">
                        <div class="comment-author-meta">
                            <h4>{{ $comment->author_name }} <small>{{ $comment->date }}</small></h4>
                        </div>
                    </div>
                    <div class="comment-content">
                        {!! $comment->body_html !!}
                    </div>
                </li>
            @endforeach

            {{--            <li class="comment-item">--}}
            {{--                <div class="comment-heading clearfix">--}}
            {{--                    <div class="comment-author-meta">--}}
            {{--                        <h4>John Doe <small>January 14, 2016</small></h4>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <div class="comment-content">--}}
            {{--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio nesciunt nulla--}}
            {{--                        est, atque ratione nostrum cumque ducimus maxime, amet enim tempore ipsam. Id--}}
            {{--                        ea, veniam ipsam perspiciatis assumenda magnam doloribus!</p>--}}
            {{--                    <p>Quibusdam iusto culpa, necessitatibus, libero sequi quae commodi ea ab non--}}
            {{--                        facilis enim vitae inventore laborum hic unde esse debitis. Adipisci nostrum--}}
            {{--                        reprehenderit explicabo, non molestias aliquid quibusdam tempore. Vel.</p>--}}

            {{--                    <ul class="comments-list-children">--}}
            {{--                        <li class="comment-item">--}}
            {{--                            <div class="comment-heading clearfix">--}}
            {{--                                <div class="comment-author-meta">--}}
            {{--                                    <h4>John Doe <small>January 14, 2016</small></h4>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                            <div class="comment-content">--}}
            {{--                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio--}}
            {{--                                    nesciunt nulla est, atque ratione nostrum cumque ducimus maxime,--}}
            {{--                                    amet enim tempore ipsam. Id ea, veniam ipsam perspiciatis assumenda--}}
            {{--                                    magnam doloribus!</p>--}}
            {{--                                <p>Quibusdam iusto culpa, necessitatibus, libero sequi quae commodi ea--}}
            {{--                                    ab non facilis enim vitae inventore laborum hic unde esse debitis.--}}
            {{--                                    Adipisci nostrum reprehenderit explicabo, non molestias aliquid--}}
            {{--                                    quibusdam tempore. Vel.</p>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}

            {{--                        <li class="comment-item">--}}
            {{--                            <div class="comment-heading clearfix">--}}
            {{--                                <div class="comment-author-meta">--}}
            {{--                                    <h4>John Doe <small>January 14, 2016</small></h4>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                            <div class="comment-content">--}}
            {{--                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio--}}
            {{--                                    nesciunt nulla est, atque ratione nostrum cumque ducimus maxime,--}}
            {{--                                    amet enim tempore ipsam. Id ea, veniam ipsam perspiciatis assumenda--}}
            {{--                                    magnam doloribus!</p>--}}
            {{--                                <p>Quibusdam iusto culpa, necessitatibus, libero sequi quae commodi ea--}}
            {{--                                    ab non facilis enim vitae inventore laborum hic unde esse debitis.--}}
            {{--                                    Adipisci nostrum reprehenderit explicabo, non molestias aliquid--}}
            {{--                                    quibusdam tempore. Vel.</p>--}}

            {{--                                <ul class="comments-list-children">--}}
            {{--                                    <li class="comment-item">--}}
            {{--                                        <div class="comment-heading clearfix">--}}
            {{--                                            <div class="comment-author-meta">--}}
            {{--                                                <h4>John Doe <small>January 14, 2016</small></h4>--}}
            {{--                                            </div>--}}
            {{--                                        </div>--}}
            {{--                                        <div class="comment-content">--}}
            {{--                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.--}}
            {{--                                                Optio nesciunt nulla est, atque ratione nostrum cumque--}}
            {{--                                                ducimus maxime, amet enim tempore ipsam. Id ea, veniam--}}
            {{--                                                ipsam perspiciatis assumenda magnam doloribus!</p>--}}
            {{--                                            <p>Quibusdam iusto culpa, necessitatibus, libero sequi quae--}}
            {{--                                                commodi ea ab non facilis enim vitae inventore laborum--}}
            {{--                                                hic unde esse debitis. Adipisci nostrum reprehenderit--}}
            {{--                                                explicabo, non molestias aliquid quibusdam tempore.--}}
            {{--                                                Vel.</p>--}}
            {{--                                        </div>--}}
            {{--                                    </li>--}}
            {{--                                </ul>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--            </li>--}}
        </ul>

        <nav>
            {!! $postComments->links() !!}
        </nav>
    </div>

    <div class="comment-footer padding-10">
        <h3>Leave a comment</h3>
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        {!! Form::open(['route' => ['blog.comments', $post->slug]]) !!}
        <form>
            <div class="form-group required {{ $errors->has('author_name') ? 'has-error' : '' }}">
                <label for="name">Name</label>
                {!! Form::text('author_name', null, ['class' => 'form-control']) !!}
                @if ($errors->has('author_name'))
                    <span class="help-block">{{ $errors->first('author_name') }}</span>
                @endif
            </div>
            <div class="form-group required {{ $errors->has('author_email') ? 'has-error' : '' }}">
                <label for="email">Email</label>
                {!! Form::text('author_email', null, ['class' => 'form-control']) !!}
                @if ($errors->has('author_email'))
                    <span class="help-block">{{ $errors->first('author_email') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="website">Website</label>
                {!! Form::text('author_url', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group required {{ $errors->has('body') ? 'has-error' : '' }}">
                <label for="comment">Comment</label>
                {!! Form::textarea('body', null, ['rows' => 5, 'class' => 'form-control']) !!}
                @if ($errors->has('body'))
                    <span class="help-block">{{ $errors->first('body') }}</span>
                @endif
            </div>
            <div class="clearfix">
                <div class="pull-left">
                    <button type="submit" class="btn btn-lg btn-success">Submit</button>
                </div>
                <div class="pull-right">
                    <p class="text-muted">
                        <span class="required">*</span>
                        <em>Indicates required fields</em>
                    </p>
                </div>
            </div>
        </form>
        {!! Form::close() !!}
    </div>

</article>
