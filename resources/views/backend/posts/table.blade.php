<table class="table table-bordered">
    <thead>
    <tr>
        <td>Title</td>
        <td>Author</td>
        <td>Category</td>
        <td>Date</td>
        <td>Action</td>
    </tr>
    </thead>
    <tbody>
    @php $request = request(); @endphp
    @foreach($posts as $post)
        <tr>
            <td>{{ $post->title }}</td>
            <td>{{ $post->author->name }}</td>
            <td>{{ $post->category->title }}</td>
            <td>
                <abbr
                    title="{{ $post->dateFormatted(true) }}">{{ $post->dateFormatted() }}</abbr>
                |
                {!!  $post->publishcationLabel()  !!}
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id]]) !!}
                @if (check_user_permissions($request, "Post@edit", $post->id))
                    <a href="{{ route('posts.edit', $post->id) }}"
                       class="btn btn-xs btn-default"><i
                            class="fa fa-edit"></i></a>
                @else
                    <a href="#"
                       class="btn btn-xs btn-default disabled"><i
                            class="fa fa-edit"></i></a>
                @endif

                @if (check_user_permissions($request, "Post@destroy", $post->id))
                    <button type="submit"
                            class="btn btn-xs btn-danger"><i
                            class="fa fa-trash"></i></button>
                @else
                    <button type="button" onclick="return false;"
                            class="btn btn-xs btn-danger disabled"><i
                            class="fa fa-trash"></i></button>
                @endif
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
