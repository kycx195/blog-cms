@extends('layouts.backend.main')
@section('title', 'MyBlog | Add New Post')
@section('content')
    <section class="content-header">
        <h1>
            Posts
            <small>Add new Post</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('posts.index') }}">Blog</a>
            </li>
            <li class="active">
                Add New Posts
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::model($post, [
                           'method' => 'POST',
                           'route' => 'posts.store',
                           'files' => TRUE,
                           'id' => 'post-form'
                       ]) !!}
            @include('backend.posts.form')
            {!! Form::close() !!}
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
@include('backend.posts.script')
