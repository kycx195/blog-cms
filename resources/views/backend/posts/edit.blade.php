@extends('layouts.backend.main')
@section('title', 'MyBlog | Edit Post')
@section('content')
    <section class="content-header">
        <h1>
            Posts
            <small>Edit Post</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('posts.index') }}">Blog</a>
            </li>
            <li class="active">
                Add New Posts
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::model($post, [
                           'method' => 'PUT',
                           'route' => ['posts.update', $post->id],
                           'files' => TRUE,
                           'id' => 'post-form'
                       ]) !!}
            @include('backend.posts.form')
            {!! Form::close() !!}
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
@include('backend.posts.script')
