@push('styles')
    <link rel="stylesheet" href="/backend/plugins/tag-editor/jquery.tag-editor.css">
@endpush
@push('scripts')
    <script src="/backend/plugins/tag-editor/jquery.caret.min.js"></script>
    <script src="/backend/plugins/tag-editor/jquery.tag-editor.min.js"></script>
    <script type="text/javascript">
        let options = {};
        @if($post->exists)
            options = {
            initialTags: {!! $post->tags_list !!},
        };
        @endif
        $('input[name=post_tags]').tagEditor(options);
        $('ul.pagination').addClass('no-margin pagination-sm');
        $('#title').on('blur', function () {
            let theTitle = this.value.toLowerCase().trim();
            let slugInput = $('#slug');
            let theSlug = theTitle.replace(/[^a-z0-9-]+/g, '-')
                .replace(/\-\-+/g, '-')
                .replace(/^-+|-+$/g, '')
                .replace(/&/g, '-and-');
            slugInput.val(theSlug);
        });

        let simplemde1 = new SimpleMDE({element: $('#excerpt')[0]})
        let simplemde2 = new SimpleMDE({element: $('#body')[0]})

        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            showClear: true
        });

        $('#draft-btn').click(function (e) {
            e.preventDefault();
            $('#published_at').val('');
            $('#post-form').submit();
        });
    </script>
@endpush
