@extends('layouts.backend.main')
@section('title', 'MyBlog | Posts')
@section('content')
    <section class="content-header">
        <h1>
            Posts
            <small>All Blog Posts</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('posts.index') }}">Blog</a>
            </li>
            <li class="active">
                Posts
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('posts.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add
                                New Post</a>
                        </div>
                        <div class="pull-right">
                            @php $links = [] @endphp
                            @foreach($statusList as $key => $value)
                                @if($value)
                                    @php $selected = Request::get('status') == $key ? 'selected-status' : '' @endphp
                                    @php $links[] = "<a class=\"{$selected}\" href=\"?status={$key}\">".ucwords($key)."({$value})</a>" @endphp
                                @endif
                            @endforeach
                            {!! implode(' | ', $links) !!}
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                        @include('backend.partials.message')
                        @if (!$postsCount)
                            <div class="alert alert-warning">
                                <strong>No record found</strong>
                            </div>
                        @else
                            @if($onlyTrashed)
                                @include('backend.posts.table-trash')
                            @else
                                @include('backend.posts.table')
                            @endif
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <div class="pull-left">
                            {!! $posts->appends(Request::query())->links() !!}
                        </div>
                        <div class="pull-right">
                            <small>{{ $postsCount }} {{ Str::plural('Item', $postsCount) }}</small>
                        </div>
                    </div>
                    @endif
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script type="text/javascript">
        $('ul.pagination').addClass('no-margin pagination-sm');
    </script>
@endpush
