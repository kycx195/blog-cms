<table class="table table-bordered">
    <thead>
    <tr>
        <td>Title</td>
        <td>Author</td>
        <td>Category</td>
        <td>Date</td>
        <td>Action</td>
    </tr>
    </thead>
    <tbody>
    @php $request = request(); @endphp
    @foreach($posts as $post)
        <tr>
            <td>{{ $post->title }}</td>
            <td>{{ $post->author->name }}</td>
            <td>{{ $post->category->title }}</td>
            <td>
                <abbr title="{{ $post->dateFormatted(true) }}">{{ $post->dateFormatted() }}</abbr>
            </td>
            <td>
                {!! Form::open(['style' => 'display: inline-block', 'method' => 'PUT', 'route' => ['posts.restore', $post->id]]) !!}
                @if (check_user_permissions($request, "Post@restore", $post->id))
                    <button title="restore" type="submit"
                            class="btn btn-xs btn-default">
                        <i class="fa fa-refresh"></i></button>
                @else
                    <button title="restore" type="button" onclick="return false;"
                            class="btn btn-xs btn-default disabled">
                        <i class="fa fa-refresh"></i></button>
                @endif
                {!! Form::close() !!}
                {!! Form::open(['style' => 'display: inline-block', 'method' => 'DELETE', 'route' => ['posts.force-destroy', $post->id]]) !!}
                @if (check_user_permissions($request, "Post@forceDestroy", $post->id))
                    <button title="Delete Permanent"
                            onclick="return confirm('You are about to delete a post permanently. Are you sure?')"
                            type="submit"
                            class="btn btn-xs btn-danger">
                        <i class="fa fa-times"></i></button>
                @else
                    <button title="Delete Permanent"
                            onclick="return false;"
                            type="button"
                            class="btn btn-xs btn-danger disabled">
                        <i class="fa fa-times"></i></button>
                @endif
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
