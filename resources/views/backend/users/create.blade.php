@extends('layouts.backend.main')
@section('title', 'MyBlog | Add New User')
@section('content')
    <section class="content-header">
        <h1>
            Users
            <small>Add new User</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('users.index') }}">Users</a>
            </li>
            <li class="active">
                Add New User
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::model($user, [
                           'method' => 'POST',
                           'route' => 'users.store',
                           'id' => 'user-form'
                       ]) !!}
            @include('backend.users.form')
            {!! Form::close() !!}
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
