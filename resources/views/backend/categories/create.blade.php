@extends('layouts.backend.main')
@section('title', 'MyBlog | Add New Category')
@section('content')
    <section class="content-header">
        <h1>
            Categories
            <small>Add new Category</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('categories.index') }}">Categories</a>
            </li>
            <li class="active">
                Add New Category
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            {!! Form::model($category, [
                           'method' => 'POST',
                           'route' => 'categories.store',
                           'id' => 'category-form'
                       ]) !!}
            @include('backend.categories.form')
            {!! Form::close() !!}
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
@include('backend.categories.script')
