@extends('layouts.backend.main')
@section('title', 'MyBlog | Categories')
@section('content')
    <section class="content-header">
        <h1>
            Categories
            <small>All Blog Categories</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ route('categories.index') }}">Categories</a>
            </li>
            <li class="active">
                Categories
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('categories.create') }}" class="btn btn-primary"><i
                                    class="fa fa-plus"></i> Add
                                New Category</a>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                        @include('backend.partials.message')
                        @if (!$categoriesCount)
                            <div class="alert alert-warning">
                                <strong>No record found</strong>
                            </div>
                        @else
                            @include('backend.categories.table')
                        @endif
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <div class="pull-left">
                            {!! $categories->appends(Request::query())->links() !!}
                        </div>
                        <div class="pull-right">
                            <small>{{ $categoriesCount }} {{ Str::plural('Item', $categoriesCount) }}</small>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- ./row -->
    </section>
    <!-- /.content -->
@endsection
@push('scripts')
    <script type="text/javascript">
        $('ul.pagination').addClass('no-margin pagination-sm');
    </script>
@endpush
