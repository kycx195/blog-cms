@push('scripts')
    <script type="text/javascript">
        $('#title').on('blur', function () {
            let theTitle = this.value.toLowerCase().trim();
            let slugInput = $('#slug');
            let theSlug = theTitle.replace(/[^a-z0-9-]+/g, '-')
                .replace(/\-\-+/g, '-')
                .replace(/^-+|-+$/g, '')
                .replace(/&/g, '-and-');
            slugInput.val(theSlug);
        });
    </script>
@endpush
