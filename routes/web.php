<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);
Route::get('/home', 'Backend\HomeController@index')->name('home');
Route::get('/edit-account', 'Backend\HomeController@edit');
Route::put('/edit-account', 'Backend\HomeController@update');
Route::put('/backend/posts/restore/{post}', 'Backend\PostController@restore')->name('posts.restore');
Route::delete('/backend/posts/force-destroy/{post}', 'Backend\PostController@forceDestroy')->name('posts.force-destroy');
Route::resource('/backend/posts', 'Backend\PostController');
Route::resource('/backend/categories', 'Backend\CategoryController');
Route::get('backend/users/confirm/{user}', 'Backend\UserController@confirm')->name('users.confirm');
Route::resource('/backend/users', 'Backend\UserController');

Route::get('/', 'BlogController@index')->name('blog.index');
Route::get('/{posts}', 'BlogController@show')->name('blog.show');
Route::post('/{posts}/comments', 'CommentController@store')->name('blog.comments');
Route::get('/category/{category}', 'BlogController@getByCategory')->name('blog.category');
Route::get('/author/{author}', 'BlogController@getByAuthor')->name('blog.author');
Route::get('/tag/{tag}', 'BlogController@getByTag')->name('blog.tag');
