<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    protected $limit = 3;

    public function index()
    {
        $posts = Post::with('author', 'category', 'tags', 'comments')
            ->lastestFirst()
            ->published()
            ->filter(request()->only(['term', 'year', 'month']))
            ->simplePaginate($this->limit);
        return view('blog.index', compact('posts'));
    }

    public function show(Post $post)
    {
        $post->increment('view_count');
        $postComments = $post->comments()->simplePaginate(3);
        return view('blog.show', compact('post', 'postComments'));
    }

    public function getByCategory(Category $category)
    {
        $categoryName = $category->title;
        $posts = $category->posts()->with('author', 'tags', 'comments')->lastestFirst()->published()->simplePaginate($this->limit);
        return view('blog.index', compact('posts', 'categoryName'));
    }

    public function getByAuthor(User $author)
    {
        $authorName = $author->name;
        $posts = $author->posts()->with('category', 'tags', 'comments')->lastestFirst()->published()->simplePaginate($this->limit);
        return view('blog.index', compact('posts', 'authorName'));
    }

    public function getByTag(Tag $tag)
    {
        $tagName = $tag->name;
        $posts = $tag->posts()->with('author', 'category', 'comments')->lastestFirst()->published()->simplePaginate($this->limit);
        return view('blog.index', compact('posts', 'tagName'));
    }
}
