<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccountUpdateRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;

class HomeController extends BackendController
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend.home.home');
    }

    public function edit(Request $request)
    {
        $user = $request->user();
        return view('backend.home.edit', compact('user'));
    }

    public function update(AccountUpdateRequest $request)
    {
        $user = $request->user();
        $user->update($request->all());

        return redirect()->back()->with('message', 'Account was update successfully!');
    }
}
