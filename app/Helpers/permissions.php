<?php

use App\Models\Post;

function check_user_permissions($request, $actionName = NULL, $id = NULL)
{
    $currentUser = $request->user();
    if ($actionName) {
        $currentActionName = $actionName;
    } else {
        $currentActionName = $request->route()->getActionName();
    }
    list($controller, $method) = explode('@', $currentActionName);
    $controller = str_replace(["App\\Http\\Controllers\\Backend\\", "Controller"], "", $controller);

    $curdPermissionsMap = [
        'crud' => ['create', 'store', 'edit', 'update', 'destroy', 'restore', 'forceDestroy', 'index', 'view']
    ];

    $classesMap = [
        'Post' => 'post',
        'Category' => 'category',
        'User' => 'user'
    ];

    foreach ($curdPermissionsMap as $permission => $methods) {
        if (in_array($method, $methods) && isset($classesMap[$controller])) {
            $className = $classesMap[$controller];
            if ($className == 'post' && in_array($method, ['edit', 'update', 'destroy', 'restore', 'forceDestroy'])) {
                $id = !is_null($id) ? $id : $request->post;
                if ($id && (!$currentUser->isAbleTo('update-others-post')) || (!$currentUser->isAbleTo('delete-others-post'))) {
                    $post = Post::withTrashed()->find($id);
                    if ($post->author_id !== $currentUser->id) {
                        return false;
                    }
                }
            }
            if (!$currentUser->isAbleTo("{$permission}-{$className}")) {
                return false;
            }
            break;
        }
    }
    return true;
}
