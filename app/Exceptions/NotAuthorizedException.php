<?php

namespace App\Exceptions;

use Exception;

class NotAuthorizedException extends Exception
{
    protected $route;
    protected $keyMessage;
    protected $message;

    public function redirectTo($route)
    {
        $this->route = $route;

        return $this;
    }

    public function setMessage($key, $message)
    {
        $this->keyMessage = $key;
        $this->message = $message;
        return $this;
    }

    public function route()
    {
        return $this->route;
    }

    public function keyMessage()
    {
        return $this->keyMessage;
    }

    public function message()
    {
        return $this->message;
    }
}
