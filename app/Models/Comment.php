<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GrahamCampbell\Markdown\Facades\Markdown;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'author_name', 'author_email', 'author_url', 'body', 'post_id'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    function getBodyHtmlAttribute()
    {
        return Markdown::convertToHtml($this->body);
    }

    function getDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
