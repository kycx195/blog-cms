<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();

        $faker = Factory::create();

        DB::table('users')->insert([
            [
                'name' => 'admin',
                'slug' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('123456'),
                'bio' => $faker->text(rand(250, 300))
            ],
            [
                'name' => 'editor',
                'slug' => 'editor',
                'email' => 'editor@gmail.com',
                'password' => bcrypt('123456'),
                'bio' => $faker->text(rand(250, 300))
            ],
            [
                'name' => 'author',
                'slug' => 'author',
                'email' => 'author@gmail.com',
                'password' => bcrypt('123456'),
                'bio' => $faker->text(rand(250, 300))
            ]
        ]);
    }
}
